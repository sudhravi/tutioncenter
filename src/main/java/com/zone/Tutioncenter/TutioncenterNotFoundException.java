package com.zone.Tutioncenter;

public class TutioncenterNotFoundException extends RuntimeException {

	public TutioncenterNotFoundException(String exception) {
		super(exception);
	}

}