package com.zone.Tutioncenter;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TutioncenterResource {
@Autowired
private TutioncenterRepository tutionRepo;

@GetMapping("/tution")
public List<Tutioncenter> retieveAll(){
	return tutionRepo.findAll();
}
@GetMapping("/tutionname/{studentName}")
public Tutioncenter retievebyName(@PathVariable String studentName){
	return tutionRepo.findByStudentName(studentName);
}
@GetMapping("/tutionsclass/{studentClass}")
public List<Tutioncenter> retieveByClass(@PathVariable String studentClass){
	return tutionRepo.findByStudentClass(studentClass);
}
@GetMapping("/tutionboard/{studentBoard}")
public List<Tutioncenter> retieveByBoard(@PathVariable String studentBoard){
	return tutionRepo.findByStudentBoard(studentBoard);
}
@GetMapping("/tution/{studentId}")
public  Tutioncenter retrieveById(@PathVariable Long studentId) {
	Optional<Tutioncenter> parent= tutionRepo.findById(studentId);
	if(!parent.isPresent()) {
		throw new TutioncenterNotFoundException("Id "+studentId);
	}
	return parent.get();
}
@DeleteMapping("/tution/{studentId}")
public void deleteTutioncenter(@PathVariable Long studentId) {
	tutionRepo.deleteById(studentId);
}
@PutMapping("/tution/{studentId}")
public Tutioncenter updateTutioncenter(@RequestBody Tutioncenter tc,@PathVariable Long studentId) {
	Optional<Tutioncenter> tcold=tutionRepo.findById(studentId);
	if(!tcold.isPresent()) {
		throw new TutioncenterNotFoundException("Id "+studentId);
	}
	tc.setStudentId(studentId);
	return tutionRepo.save(tc);
	
}
@PostMapping("/tution")
public Tutioncenter createTution(@RequestBody Tutioncenter tc) {
	Tutioncenter tutioncenter = tutionRepo.save(tc);

	return tutioncenter;

}
}
