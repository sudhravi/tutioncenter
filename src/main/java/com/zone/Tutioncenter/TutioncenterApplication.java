package com.zone.Tutioncenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutioncenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutioncenterApplication.class, args);
	}
}
