package com.zone.Tutioncenter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Tutioncenter {
@Id
@GeneratedValue
Long studentId;
String studentName;
String studentClass;
String studentBoard;
public Long getStudentId() {
	return studentId;
}
public void setStudentId(Long studentId) {
	this.studentId = studentId;
}
public String getStudentName() {
	return studentName;
}
public void setStudentName(String studentName) {
	this.studentName = studentName;
}
public String getStudentClass() {
	return studentClass;
}
public void setStudentClass(String studentClass) {
	this.studentClass = studentClass;
}
public String getStudentBoard() {
	return studentBoard;
}
public void setStudentBoard(String studentBoard) {
	this.studentBoard = studentBoard;
}
}
