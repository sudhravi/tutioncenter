package com.zone.Tutioncenter;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TutioncenterRepository extends JpaRepository<Tutioncenter, Long> {

	Tutioncenter findByStudentName(String studentName);
	
	List<Tutioncenter> findByStudentClass(String studentClass);

	List<Tutioncenter> findByStudentBoard(String studentBoard);

}
